import {
  Component,
  ElementRef,
  EventEmitter,
  Output,
  ViewChild,
} from '@angular/core';
import { Ingredient } from '../../shared/ingredient.model';
import { ShoppingListService } from './../shopping-list.service';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css'],
})
export class ShoppingEditComponent {
  @ViewChild('inputName', { static: false }) nameInputRef: ElementRef;
  @ViewChild('inputAmount', { static: false }) amountInputRef: ElementRef;

  @Output() onAddIngredient = new EventEmitter<Ingredient>();

  /**
   *
   */
  constructor(private shoppingListService: ShoppingListService) {}

  handleAddIngredient() {
    this.shoppingListService.addIngredients([
      {
        name: this.nameInputRef.nativeElement.value,
        amount: this.amountInputRef.nativeElement.value,
      },
    ]);
  }
}
