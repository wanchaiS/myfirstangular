import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';
import { ShoppingListService } from './../../shopping-list/shopping-list.service';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css'],
})
export class RecipeDetailComponent implements OnInit {
  @Input() editRecipe: Recipe;

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.editRecipe = this.recipeService.getRecipe(params['id']);
    });
  }
  /**
   *
   */
  constructor(
    private shoppingListService: ShoppingListService,
    private recipeService: RecipeService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  handleAddToShoppingList() {
    this.shoppingListService.addIngredients(this.editRecipe.ingredients);
  }

  handleEditRecipe() {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }
}
