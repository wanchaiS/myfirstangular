import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css'],
})
export class RecipeEditComponent implements OnInit {
  editMode: boolean;
  @Input() editRecipe: Recipe;

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.editMode = !!params['id'];
      this.editRecipe = this.recipeService.getRecipe(params['id']);
    });
  }

  constructor(
    private recipeService: RecipeService,
    private route: ActivatedRoute
  ) {}
}
