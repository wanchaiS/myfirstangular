import { Component } from '@angular/core';
import { RecipeService } from './recipe.service';

import { Recipe } from './recipe.model';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css'],
})
export class RecipesComponent {
  activeRecipe: Recipe;

  /**
   *
   */
  constructor(private recipeService: RecipeService) {}
}
