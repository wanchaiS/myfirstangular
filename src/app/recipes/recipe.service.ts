import { Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { Recipe } from './recipe.model';

@Injectable({ providedIn: 'root' })
export class RecipeService {
  private recipes: Recipe[] = [
    new Recipe(
      1,
      'A Test Recipe',
      'This is simply a test',
      'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F44%2F2020%2F03%2F03%2F7782449.jpg',
      [new Ingredient('Meat', 10), new Ingredient('Mushroom', 5)]
    ),
    new Recipe(
      2,
      'A Test Recipe 2',
      'This is simply a test 2',
      'https://assets.bonappetit.com/photos/63f66b68d690ce0a8efeb48a/5:7/w_2319,h_3247,c_limit/022223-spaetzle-lede.jpg',
      [new Ingredient('Chili', 10), new Ingredient('Bokchoi', 5)]
    ),
  ];

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(id: number | string) {
    const recipe = this.recipes.find((s) => {
      return s.id === +id;
    });
    return recipe;
  }
}
